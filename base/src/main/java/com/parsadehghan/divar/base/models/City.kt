package com.parsadehghan.divar.base.models

data class City(
    val id : Int = 0,
    val name : String = ""
)
