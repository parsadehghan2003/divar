package com.parsadehghan.divar.database.data_source.service

import com.parsadehghan.divar.database.domain.RealmPlace
import io.realm.kotlin.TypedRealm
import io.realm.kotlin.types.RealmList

interface PlaceDataStorage {
    suspend fun insertOrUpdatePlaces(
        realmList: RealmList<RealmPlace>,
        realm: TypedRealm? = null
    ): RealmList<RealmPlace>?

    suspend fun readPlaces(realm: TypedRealm? = null): RealmList<RealmPlace>?
}