package com.parsadehghan.divar.database.di

import android.content.Context
import com.parsadehghan.divar.database.SingleRunner
import com.parsadehghan.divar.database.data_source.service.PlaceDataStorage
import com.parsadehghan.divar.database.data_source.service.PostDataStorage
import com.parsadehghan.divar.database.domain.RealmPlace
import com.parsadehghan.divar.database.domain.RealmPost
import com.parsadehghan.divar.database.framework.DatabaseQueue
import com.parsadehghan.divar.database.framework.PlaceDataStorageImpl
import com.parsadehghan.divar.database.framework.PostDataStorageImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.realm.kotlin.RealmConfiguration
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExecutorCoroutineDispatcher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.plus
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabaseScope(): CoroutineScope = GlobalScope + Dispatchers.IO

    @Provides
    @Singleton
    fun provideDatabaseCoroutineQueue(): ExecutorCoroutineDispatcher =
        Executors.newSingleThreadExecutor().asCoroutineDispatcher()

    @Provides
    @Singleton
    fun provideSingleRunning(): SingleRunner =
        SingleRunner()

    @Provides
    @Singleton
    fun provideRealmConfig(
        @ApplicationContext context: Context,
    ): RealmConfiguration {
        return RealmConfiguration.Builder(setOf(RealmPlace::class,RealmPost::class))
            .name("Divar")
            .schemaVersion(0)
            .build()
    }

    @Provides
    @Singleton
    fun providePlaceDataStorage(
        databaseQueue: DatabaseQueue
    ): PlaceDataStorage =
        PlaceDataStorageImpl(databaseQueue)

    @Provides
    @Singleton
    fun providePostDataStorage(
        databaseQueue: DatabaseQueue
    ): PostDataStorage =
        PostDataStorageImpl(databaseQueue)

    @Provides
    @Singleton
    fun provideDatabaseQueue(
        databaseScope: CoroutineScope,
        databaseQueue: ExecutorCoroutineDispatcher,
        realmConfiguration: RealmConfiguration,
        singleRunner: SingleRunner
    ): DatabaseQueue = DatabaseQueue(databaseScope, databaseQueue, realmConfiguration)


}