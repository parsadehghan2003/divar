package com.parsadehghan.divar.post.data_source.service

import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState

interface PostRemoteService {
    suspend fun getPosts(baseDomain: BaseDomain): DataState<BaseDomain>
}