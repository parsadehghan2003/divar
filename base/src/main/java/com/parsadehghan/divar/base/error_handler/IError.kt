package com.parsadehghan.divar.base.error_handler

import com.parsadehghan.divar.base.error_handler.ErrorModel

interface IError {
    fun createError() : ErrorModel
}