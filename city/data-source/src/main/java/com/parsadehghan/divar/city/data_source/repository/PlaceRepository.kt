package com.parsadehghan.divar.city.data_source.repository

import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState

interface PlaceRepository {
    suspend fun getPlaces(): DataState<BaseDomain>
    suspend fun getCurrentPlace(): DataState<BaseDomain>
}