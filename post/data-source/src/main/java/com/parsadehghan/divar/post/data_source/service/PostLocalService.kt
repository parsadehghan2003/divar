package com.parsadehghan.divar.post.data_source.service

import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState

interface PostLocalService {
    suspend fun readPosts(): DataState<BaseDomain>
    suspend fun insertPosts(baseDomain: BaseDomain): DataState<BaseDomain>
    suspend fun readCurrentPlace(): DataState<BaseDomain>

}