package com.parsadehghan.divar.post.data_source.repository

import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState

interface PostRepository {
    suspend fun getPosts(baseDomain: BaseDomain): DataState<BaseDomain>
}