package com.parsadehghan.divar.base.models

data class PostData(
    var title: String = "",
    var token: String = "",
    var price: String = "",
    var thumbnail: String = "",
    var city: String = "",
    var district: String = "",
    var text: String = ""
)