package com.parsadehghan.divar.database.domain

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey


open class RealmPlace : RealmObject {

    @PrimaryKey
    var id: Int = 0

    var name: String? = null

    var slug: String? = null

    var radius: Long = 0

    var latitude: Double = 0.0

    var longitude: Double = 0.0

}