package com.parsadehghan.city.framework.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.core.handlers.ReplaceFileCorruptionHandler
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.preferencesDataStoreFile
import com.parsadehghan.city.framework.PlaceLocalServiceImpl
import com.parsadehghan.city.framework.PlaceRemoteServiceImpl
import com.parsadehghan.divar.city.data_source.repository.PlaceRepository
import com.parsadehghan.divar.city.data_source.repository.PlaceRepositoryImpl
import com.parsadehghan.divar.city.data_source.service.PlaceLocalService
import com.parsadehghan.divar.city.data_source.service.PlaceRemoteService
import com.parsadehghan.divar.database.data_source.service.PlaceDataStorage
import com.parsadehghan.divar.gateway.DivarApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton


@Module
@InstallIn(ViewModelComponent::class)
object PlaceFrameWorkModule {
    const val PLACE_PREFERENCES = "place_preferences"

    @Provides
    @ViewModelScoped
    fun providePlaceLocalService(
        placeDataStorage: PlaceDataStorage,
        dataStore: DataStore<Preferences>
    ): PlaceLocalService {
        return PlaceLocalServiceImpl(placeDataStorage, dataStore)
    }

    @Provides
    @ViewModelScoped
    fun providePlaceRemoteService(divarApiService: DivarApiService): PlaceRemoteService {
        return PlaceRemoteServiceImpl(divarApiService)
    }

}