package com.parsadehghan.divar.base.error_handler

import com.parsadehghan.divar.base.ActionType
import com.parsadehghan.divar.base.BaseDomain

private const val NO_CONNECTION_ERROR_MESSAGE = "No connection!"
private const val BAD_RESPONSE_ERROR_MESSAGE = "Bad response!"
private const val TIME_OUT_ERROR_MESSAGE = "Time out!"
private const val EMPTY_RESPONSE_ERROR_MESSAGE = "Empty response!"
private const val NOT_DEFINED_ERROR_MESSAGE = "Not defined!"
private const val UNAUTHORIZED_ERROR_MESSAGE = "Unauthorized!"
private const val CANCELED_BY_USER = "Cancel by user!"

data class ErrorModel(
    var errorStatus: ErrorStatus = ErrorStatus.NONE,
    var message: String? = null,
    val errorType: ErrorType = ErrorType.NOT_DEFINED,
    override val actionType: ActionType = ActionType.ERROR

):BaseDomain {

    enum class ErrorStatus {
        /**
         * error in connecting to repository (Server or Database)
         */
        NO_CONNECTION,

        /**
         * error in getting value (Json Error, Server Error, etc)
         */
        BAD_RESPONSE,

        /**
         * Time out  error
         */
        TIMEOUT,

        /**
         * no data available in repository
         */
        EMPTY_RESPONSE,

        /**
         * an unexpected error
         */
        NOT_FOUND,

        /**
         * bad credential
         */
        NOT_DEFINED,

        UNAUTHORIZED,

        /**
         * canceled by user
         */
        CANCELED,

        /**
         * major,minor 5,1
         */
        BAD_REQUEST,

        /**
         * default status
         */
        NONE,

        /**
         * major,minor 5,1
         */
        NULL_POINTER,

        CAN_NOT_CAST,

        CAN_NOT_MAP,

        CANCELED_FLOW,

        DATA_STORE_ERROR

    }
}