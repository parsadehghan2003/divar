package com.parsadehghan.divar.city.domain

import com.parsadehghan.divar.base.ActionType
import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.models.City
import com.parsadehghan.divar.base.models.PlaceObject

sealed class CRUDCityObject : BaseDomain {


    data class SetCityObjectRequest(
        val placeObjects: List<PlaceObject>,
    ) : CRUDCityObject() {
        override val actionType: ActionType
            get() = ActionType.GET_PLACES_RESPONSE

    }
    data class GetCityObjectResponse(
        val city: City,
    ) : CRUDCityObject() {
        override val actionType: ActionType
            get() = ActionType.GET_PLACES_RESPONSE

    }
}
