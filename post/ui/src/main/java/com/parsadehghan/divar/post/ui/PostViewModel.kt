package com.parsadehghan.divar.post.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState
import com.parsadehghan.divar.post.domain.GetPostsObject
import com.parsadehghan.post.usecase.GetPostsInteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PostViewModel @Inject constructor(
    private val getPostsInteractor: GetPostsInteractor
) : ViewModel() {
    val postsLiveData = MutableLiveData<DataState<BaseDomain>>()

    init {
    }

    fun getPostList(baseDomain: BaseDomain) {
        viewModelScope.launch {
            getPostsInteractor.call(baseDomain).onEach {
                postsLiveData.value = it
            }.launchIn(viewModelScope)
        }

    }

}