package com.parsadehghan.divar.city.domain

import com.parsadehghan.divar.base.ActionType
import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.models.PlaceObject

sealed class GetPlacesObject : BaseDomain {


    data class GetPlacesObjectResponse(
        val placeObjects: List<PlaceObject>,
    ) : GetPlacesObject() {
        override val actionType: ActionType
            get() = ActionType.GET_PLACES_RESPONSE

    }
}
