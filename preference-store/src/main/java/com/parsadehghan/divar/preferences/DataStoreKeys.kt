package com.parsadehghan.divar.preferences

import androidx.datastore.preferences.core.Preferences

inline val Preferences.cityId: Int?
    get() = this[CityKeys.currentCityId]

inline val Preferences.currentCityName: String?
    get() = this[CityKeys.currentCityName]



