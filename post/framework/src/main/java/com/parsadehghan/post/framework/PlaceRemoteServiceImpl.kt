package com.parsadehghan.post.framework

import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState
import com.parsadehghan.divar.base.error_handler.dataStateRemoteErrorHandler
import com.parsadehghan.divar.gateway.DivarApiService
import com.parsadehghan.divar.post.data_source.service.PostRemoteService
import com.parsadehghan.divar.post.domain.GetPostsObject
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import javax.inject.Inject


class PlaceRemoteServiceImpl @Inject constructor(
    private val divarApiService: DivarApiService,
) : PostRemoteService {
    override suspend fun getPosts(baseDomain: BaseDomain): DataState<BaseDomain> {
        return try {
            val getPostsObjectRequest = baseDomain as GetPostsObject.GetPostsObjectRequest
            val requestBody: RequestBody = JSONObject(
                "{\"page\": 1,\"last_post_date\": 0  }"
            )
                .toString()
                .toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())
            val response = divarApiService.getPostList(getPostsObjectRequest.cityId, requestBody)
            if (response.isSuccessful)
                DataState.Data(response.body()?.let { GetPostsObject.GetPostsObjectResponse(it) })
            else dataStateRemoteErrorHandler(response.code())
        } catch (exception: Exception) {
            dataStateRemoteErrorHandler(0)
        }

    }
}