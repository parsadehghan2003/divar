apply {
    from("$rootDir/android-library-build.gradle")
}
dependencies {
    "implementation"(DataStore.dataStore)
    "implementation"(DataStore.dataStorePreferencesCore)

}