package com.parsadehghan.divar.base.models

data class PlaceObject(
    var name: String = "",
    var id: Int = 0,
    var slug: String = "",
    var radius: Long = 0,
    var centroidObject: CentroidObject? = null
)