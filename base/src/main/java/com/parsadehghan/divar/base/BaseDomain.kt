package com.parsadehghan.divar.base

interface BaseDomain {
    val actionType: ActionType
}

enum class ActionType {
    ERROR,
    GET_PLACES_RESPONSE,
    GET_PLACES_REQUEST,
    GET_POSTS_RESPONSE,
    GET_POSTS_REQUEST
}
