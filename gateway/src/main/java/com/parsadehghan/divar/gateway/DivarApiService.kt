package com.parsadehghan.divar.gateway

import com.parsadehghan.divar.base.models.PlacesObject
import com.parsadehghan.divar.base.models.PostsObject
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query


interface DivarApiService {

    @GET("place/list")
    suspend fun getPlaces(): Response<PlacesObject>

    @POST("post/list")
    suspend fun getPostList(
        @Query("city") cityId: Int,
        @Body requestBody: RequestBody
    ): Response<PostsObject>

}