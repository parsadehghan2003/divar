package com.parsadehghan.divar.database.framework

import com.parsadehghan.divar.database.data_source.service.PlaceDataStorage
import com.parsadehghan.divar.database.domain.RealmPlace
import io.realm.kotlin.TypedRealm
import io.realm.kotlin.ext.toRealmList
import io.realm.kotlin.types.RealmList
import javax.inject.Inject

class PlaceDataStorageImpl @Inject constructor(
    private val databaseQueue: DatabaseQueue
) : PlaceDataStorage {

    override suspend fun insertOrUpdatePlaces(
        realmList: RealmList<RealmPlace>,
        realm: TypedRealm?
    ): RealmList<RealmPlace>? = databaseQueue.writeQueue(realm) {
        realmList
    }

    override suspend fun readPlaces(realm: TypedRealm?) =
        databaseQueue.readQueue(realm) {
            copyFromRealm(where(RealmPlace::class).findAll()).toRealmList()
        }


}