package com.parsadehghan.city.framework

import com.parsadehghan.divar.base.models.CentroidObject
import com.parsadehghan.divar.base.models.PlaceObject
import com.parsadehghan.divar.database.domain.RealmPlace
import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.types.RealmList


fun createPlaceObjectWithRealmPlace(realmObject: RealmPlace): PlaceObject {
    return PlaceObject().apply {
        realmObject.name?.let {
            name = it
        }
        realmObject.slug?.let {
            slug = it
        }
        id = realmObject.id
        radius = realmObject.radius
        centroidObject = CentroidObject(realmObject.latitude, realmObject.longitude)
    }
}

fun createRealmPlaceWithPlaceObject(placeObject: PlaceObject): RealmPlace {
    return RealmPlace().apply {
        id = placeObject.id
        name = placeObject.name
        slug = placeObject.slug
        radius = placeObject.radius
        placeObject.centroidObject?.let {
            latitude = it.latitude
            longitude = it.longitude
        }
    }
}

fun createPlaceRealmListWithPlaceObjectList(list: List<PlaceObject>): RealmList<RealmPlace> {
    val realmPlaces = realmListOf<RealmPlace>()
    list.map {
        realmPlaces.add(
            createRealmPlaceWithPlaceObject(it)
        )
    }
    return realmPlaces
}

fun createPlaceListListWithPlaceRealmList(list: RealmList<RealmPlace>): List<PlaceObject> {
    val placeObjects = mutableListOf<PlaceObject>()
    list.map {
        placeObjects.add(
            createPlaceObjectWithRealmPlace(it)
        )
    }
    return placeObjects
}
