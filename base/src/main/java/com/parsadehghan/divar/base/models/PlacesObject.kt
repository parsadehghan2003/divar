package com.parsadehghan.divar.base.models

data class PlacesObject(
    val cities : List<PlaceObject> = mutableListOf()
)