package com.parsadehghan.divar.post.domain

import com.parsadehghan.divar.base.ActionType
import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.models.PostsObject

sealed class GetPostsObject : BaseDomain {


    data class GetPostsObjectRequest(
        val page: Int,
        val lastPostDate: Long,
        val cityId: Int
    ) : GetPostsObject() {
        override val actionType: ActionType
            get() = ActionType.GET_POSTS_REQUEST

    }

    data class GetPostsObjectResponse(
        val posts: PostsObject,
    ) : GetPostsObject() {
        override val actionType: ActionType
            get() = ActionType.GET_POSTS_RESPONSE

    }
}
