pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
rootProject.name = "divar"
include(":app")
include(":base")
include(":navigator")
include(":base-android")

include(":gateway")
include(":database")


include(":preference-store")
include(":resource")

include(":city")
include(":city:domain")
include(":city:data-source")
include(":city:framework")
include(":city:usecase")
include(":city:ui")

include(":post")
include(":post:domain")
include(":post:data-source")
include(":post:framework")
include(":post:usecase")
include(":post:ui")


