package com.parsadehghan.divar.base.models

data class CentroidObject(
    var latitude: Double,
    var longitude: Double
)
