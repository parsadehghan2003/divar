package com.parsadehghan.divar.database.domain

import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.Index
import io.realm.kotlin.types.annotations.PrimaryKey


open class RealmPost : RealmObject {

    @Index
    var widgetType: String = ""

    var title: String = ""

    @PrimaryKey
    var token: String = ""

    var price: String = ""

    var thumbnail: String = ""

    var district: String = ""

    var text: String = ""

    var city: String = ""

}