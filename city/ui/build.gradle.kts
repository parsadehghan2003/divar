apply {
    from("$rootDir/android-library-build.gradle")

}
apply(plugin = "org.jetbrains.kotlin.android")
dependencies {
    "implementation"(project(":base-android"))
    "implementation"(project(":city:data-source"))
    "implementation"(project(":city:domain"))
    "implementation"(project(":city:framework"))
    "implementation"(project(":city:usecase"))
    "implementation"(project(":base"))
    "implementation"(project(":resource"))
    "implementation"(project(":navigator"))
}