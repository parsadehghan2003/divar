package com.parsadehghan.divar.navigator

enum class DestinationFragment {
    SPLASH_FRAGMENT,
    CITIES_FRAGMENT,
    POSTS_FRAGMENT
}