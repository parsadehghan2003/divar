package com.parsadehghan.divar.city

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.Gravity
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.parsadehghan.divar.base_android.extentions.dp
import com.parsadehghan.divar.base_android.util.LocaleController
import kotlin.math.roundToInt

@SuppressLint("ViewConstructor")
class TextCell(
    context: Context,
    private val leftPadding: Int = 23,
    dialog: Boolean = false
) :
    FrameLayout(context) {
    private var textView: TextView = TextView(context)
    private var valueTextView: TextView
    private var imageView: ImageView
    private var valueImageView: ImageView
    private var needDivider = false
    private var offsetFromImage = 71
    private var imageLeft = 21
    private var inDialogs = false
    private var dividerPaint: Paint = Paint()

    init {
        textView.setTextColor(if (dialog) Color.parseColor("#DBDBDB") else Color.parseColor("#707070"))
        textView.textSize = 16f
        textView.gravity = if (LocaleController.isRTL) Gravity.RIGHT else Gravity.LEFT
        textView.importantForAccessibility = IMPORTANT_FOR_ACCESSIBILITY_NO
        addView(textView)
        valueTextView = TextView(context)
        valueTextView.setTextColor(if (dialog) Color.parseColor("#DBDBDB") else Color.parseColor("#DBDBDB"))
        valueTextView.textSize = 14.dp().toFloat()
        valueTextView.gravity = if (LocaleController.isRTL) Gravity.LEFT else Gravity.RIGHT or Gravity.CENTER_VERTICAL
        valueTextView.importantForAccessibility = IMPORTANT_FOR_ACCESSIBILITY_NO
        addView(valueTextView)
        imageView = ImageView(context, null)
        imageView.scaleType = ImageView.ScaleType.CENTER

        addView(imageView)
        valueImageView = ImageView(context)
        valueImageView.scaleType = ImageView.ScaleType.CENTER
        addView(valueImageView)
        isFocusable = true
        dividerPaint.strokeWidth = 1f
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height: Int = 48.dp()
        valueTextView.measure(
            MeasureSpec.makeMeasureSpec(
                width - leftPadding.dp(),
                MeasureSpec.AT_MOST
            ), MeasureSpec.makeMeasureSpec(20.dp(), MeasureSpec.EXACTLY)
        )
        textView.measure(
            MeasureSpec.makeMeasureSpec(
                (width - (71 + leftPadding).dp() - valueTextView.textSize).toInt(),
                MeasureSpec.AT_MOST
            ), MeasureSpec.makeMeasureSpec(20.dp(), MeasureSpec.EXACTLY)
        )
        if (imageView.visibility == VISIBLE) {
            imageView.measure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.AT_MOST)
            )
        }
        if (valueImageView.visibility == VISIBLE) {
            valueImageView.measure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.AT_MOST)
            )
        }
        setMeasuredDimension(width, 50.dp() + if (needDivider) 1 else 0)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        val height = bottom - top
        val width = right - left
        var viewTop: Int = ((height - valueTextView.textScaleX) / 2).roundToInt()
        var viewLeft = if (LocaleController.isRTL) leftPadding.dp() else 0
        valueTextView.layout(
            viewLeft,
            viewTop,
            viewLeft + valueTextView.measuredWidth,
            viewTop + valueTextView.measuredHeight
        )
        viewTop = ((height - textView.textScaleX) / 2).roundToInt()
        viewLeft = if (LocaleController.isRTL) {
            measuredWidth - textView.measuredWidth - (if (imageView.visibility == VISIBLE) offsetFromImage else leftPadding).dp()
        } else {
            (if (imageView.visibility == VISIBLE) offsetFromImage else leftPadding).dp()
        }
        textView.layout(
            viewLeft,
            viewTop,
            viewLeft + textView.measuredWidth,
            viewTop + textView.measuredHeight
        )
        if (imageView.visibility == VISIBLE) {
            viewTop = 5.dp()
            viewLeft =
                if (!LocaleController.isRTL) imageLeft.dp() else width - imageView.measuredWidth - imageLeft.dp()
            imageView.layout(
                viewLeft,
                viewTop,
                viewLeft + imageView.measuredWidth,
                viewTop + imageView.measuredHeight
            )
        }
        if (valueImageView.visibility == VISIBLE) {
            viewTop = (height - valueImageView.measuredHeight) / 2
            viewLeft =
                if (LocaleController.isRTL) 23.dp() else width - valueImageView.measuredWidth - 23.dp()
            valueImageView.layout(
                viewLeft,
                viewTop,
                viewLeft + valueImageView.measuredWidth,
                viewTop + valueImageView.measuredHeight
            )
        }
    }

    fun setTextColor(color: Int) {
        textView.setTextColor(color)
    }

    fun setValueTextColor(color: Int) {
        valueTextView.setTextColor(color)
    }


    fun setText(text: String?, divider: Boolean) {
        imageLeft = 21
        textView.text = text
        imageView.visibility = GONE
        valueTextView.visibility = GONE
        valueImageView.visibility = GONE
        needDivider = divider
        setWillNotDraw(!needDivider)
    }

    fun setTextAndIcon(text: String?, resId: Int, divider: Boolean) {
        imageLeft = 21
        offsetFromImage = 71
        textView.text = text
        valueTextView.text = null
        imageView.setImageResource(resId)
        imageView.visibility = VISIBLE
        valueTextView.visibility = GONE
        valueImageView.visibility = GONE
        imageView.setPadding(0, 7.dp(), 0, 0)
        needDivider = divider
        setWillNotDraw(!needDivider)
    }

    fun setOffsetFromImage(value: Int) {
        offsetFromImage = value
    }

    fun setImageLeft(imageLeft: Int) {
        this.imageLeft = imageLeft
    }

    fun setTexts(text: String?, divider: Boolean) {
        imageLeft = 21
        offsetFromImage = 71
        textView.text = text
        valueTextView.visibility = GONE
        imageView.visibility = GONE
        valueImageView.visibility = GONE
        needDivider = divider
        setWillNotDraw(!needDivider)
    }

    fun setTextAndValue(text: String?, value: String?, divider: Boolean) {
        imageLeft = 21
        offsetFromImage = 71
        textView.text = text
        valueTextView.text = value
        valueTextView.visibility = VISIBLE
        imageView.visibility = GONE
        valueImageView.visibility = GONE
        needDivider = divider
        setWillNotDraw(!needDivider)
    }

    fun setTextAndValueAndIcon(text: String?, value: String?, resId: Int, divider: Boolean) {
        imageLeft = 21
        offsetFromImage = 71
        textView.text = text
        valueTextView.text = value
        valueTextView.visibility = VISIBLE
        valueImageView.visibility = GONE
        imageView.visibility = VISIBLE
        imageView.setPadding(0, 7.dp(), 0, 0)
        imageView.setImageResource(resId)
        needDivider = divider
        setWillNotDraw(!needDivider)
    }

    fun setTextAndValueDrawable(text: String?, drawable: Drawable?, divider: Boolean) {
        imageLeft = 21
        offsetFromImage = 71
        textView.text = text
        valueTextView.text = null
        valueImageView.visibility = VISIBLE
        valueImageView.setImageDrawable(drawable)
        valueTextView.visibility = GONE
        imageView.visibility = GONE
        imageView.setPadding(0, 7.dp(), 0, 0)
        needDivider = divider
        setWillNotDraw(!needDivider)
    }

    override fun onDraw(canvas: Canvas) {
        if (needDivider) {
            canvas.drawLine(
                if (LocaleController.isRTL) {
                    0F
                } else {
                    if (imageView.visibility == VISIBLE) {
                        if (inDialogs)
                            72.dp().toFloat()
                        else
                            68.dp().toFloat()
                    } else {
                        20.dp().toFloat()
                    }
                },
                (measuredHeight - 1).toFloat(),
                (measuredWidth - if (LocaleController.isRTL) {
                    if (imageView.visibility == VISIBLE) {
                        if (inDialogs)
                            72.dp().toFloat()
                        else
                            68.dp().toFloat()
                    } else {
                        20.dp().toFloat()
                    }
                } else {
                    0f
                }),

                (measuredHeight - 1).toFloat(),
                dividerPaint
            )
        }
    }

    override fun onInitializeAccessibilityNodeInfo(info: AccessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(info)
        val text: CharSequence = textView.text
        if (!TextUtils.isEmpty(text)) {
            val valueText: CharSequence = valueTextView.text
            if (!TextUtils.isEmpty(valueText)) {
                info.text = "$text: $valueText"
            } else {
                info.text = text
            }
        }

        info.addAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_CLICK)
    }

    fun setNeedDivider(needDivider: Boolean) {
        if (this.needDivider != needDivider) {
            this.needDivider = needDivider
            setWillNotDraw(!needDivider)
            invalidate()
        }
    }
}