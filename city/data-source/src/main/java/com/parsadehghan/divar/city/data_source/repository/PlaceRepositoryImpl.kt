package com.parsadehghan.divar.city.data_source.repository

import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState
import com.parsadehghan.divar.city.data_source.service.PlaceLocalService
import com.parsadehghan.divar.city.data_source.service.PlaceRemoteService
import com.parsadehghan.divar.city.domain.GetPlacesObject

class PlaceRepositoryImpl(
    private val placeLocalService: PlaceLocalService,
    private val placeRemoteService: PlaceRemoteService
) : PlaceRepository {
    override suspend fun getPlaces(): DataState<BaseDomain> {

        return when (val getPlaces = placeRemoteService.getPlaces()) {
            is DataState.Data -> {
                val getPlacesResponse = getPlaces.data as GetPlacesObject.GetPlacesObjectResponse
                placeLocalService.insertPlaces(getPlacesResponse)
                placeLocalService.readPlaces()
            }

            is DataState.Error -> {
                placeLocalService.readPlaces()
            }
        }
    }

    override suspend fun getCurrentPlace(): DataState<BaseDomain> {
        return placeLocalService.readCurrentPlace()
    }
}