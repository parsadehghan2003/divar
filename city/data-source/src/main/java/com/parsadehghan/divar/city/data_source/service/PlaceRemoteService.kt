package com.parsadehghan.divar.city.data_source.service

import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState

interface PlaceRemoteService {
    suspend fun getPlaces(): DataState<BaseDomain>
}