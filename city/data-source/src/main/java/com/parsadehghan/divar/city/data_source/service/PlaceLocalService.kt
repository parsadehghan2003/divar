package com.parsadehghan.divar.city.data_source.service

import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState

interface PlaceLocalService {
    suspend fun readPlaces(): DataState<BaseDomain>
    suspend fun insertPlaces(baseDomain: BaseDomain): DataState<BaseDomain>
    suspend fun readCurrentPlace(): DataState<BaseDomain>
}