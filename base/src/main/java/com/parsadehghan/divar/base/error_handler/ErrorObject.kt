package com.parsadehghan.divar.base.error_handler

import com.parsadehghan.divar.base.ActionType
import com.parsadehghan.divar.base.BaseDomain

data class ErrorObject(
    val type:ErrorType = ErrorType.NOT_DEFINED,
    val message: String? = null
) : BaseDomain {
    override val actionType: ActionType
        get() = ActionType.ERROR
}