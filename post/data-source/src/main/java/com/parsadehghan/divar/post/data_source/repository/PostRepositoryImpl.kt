package com.parsadehghan.divar.post.data_source.repository

import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState
import com.parsadehghan.divar.post.data_source.service.PostLocalService
import com.parsadehghan.divar.post.data_source.service.PostRemoteService
import com.parsadehghan.divar.post.domain.GetPostsObject

class PostRepositoryImpl(
    private val placeLocalService: PostLocalService,
    private val placeRemoteService: PostRemoteService
) : PostRepository {
    override suspend fun getPosts(baseDomain: BaseDomain): DataState<BaseDomain> {
        val getPostsObjectRequest = baseDomain as GetPostsObject.GetPostsObjectRequest
        return when (val getPlaces = placeRemoteService.getPosts(baseDomain)) {
            is DataState.Data -> {
                val getPlacesResponse = getPlaces.data as GetPostsObject.GetPostsObjectResponse
                if (getPostsObjectRequest.page == 0)
                    placeLocalService.insertPosts(getPlacesResponse)
                getPlaces
            }

            is DataState.Error -> {
                placeLocalService.readPosts()
            }
        }
    }

}