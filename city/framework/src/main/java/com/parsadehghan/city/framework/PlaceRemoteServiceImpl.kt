package com.parsadehghan.city.framework

import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState
import com.parsadehghan.divar.base.error_handler.dataStateInternalErrorHandler
import com.parsadehghan.divar.base.error_handler.dataStateRemoteErrorHandler
import com.parsadehghan.divar.city.data_source.service.PlaceRemoteService
import com.parsadehghan.divar.city.domain.GetPlacesObject
import com.parsadehghan.divar.gateway.DivarApiService
import javax.inject.Inject

class PlaceRemoteServiceImpl @Inject constructor(
    private val divarApiService: DivarApiService,
) : PlaceRemoteService {
    override suspend fun getPlaces(): DataState<BaseDomain> {
        return try {
            val response = divarApiService.getPlaces()
            if (response.isSuccessful && response.body() != null) DataState.Data(
                GetPlacesObject.GetPlacesObjectResponse(
                    response.body()!!.cities
                )
            ) else dataStateRemoteErrorHandler(response.code())
        } catch (e: Exception) {
            dataStateRemoteErrorHandler(0)
        }


    }
}