package com.parsadehghan.divar

import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.parsadehghan.divar.base_android.util.LayoutHelper
import com.parsadehghan.divar.navigator.DestinationFragment
import com.parsadehghan.divar.navigator.NavigationHelper
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashFragment : Fragment() {
    private lateinit var rootView: FrameLayout
    private lateinit var imageView: ImageView
    private val TAG = "SplashFragment"


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        rootView = FrameLayout(requireContext())
        rootView.layoutDirection = View.LAYOUT_DIRECTION_LTR

        imageView = ImageView(requireContext())
        imageView.background = requireContext().getDrawable(R.mipmap.ic_launcher)
        rootView.addView(
            imageView,
            LayoutHelper.createFrame(
                120,
                200,
                Gravity.CENTER
            )
        )
        NavigationHelper.navigateToDestination(
            DestinationFragment.CITIES_FRAGMENT,
            replace = true,
            addToBackStack = false,
        )

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

}