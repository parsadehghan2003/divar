package com.parsadehghan.divar.city.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.parsadehghan.city.usecase.GetPlacesInteractor
import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CityViewModel @Inject constructor(
    private val getPlacesInteractor: GetPlacesInteractor
) : ViewModel() {
    val placeListLiveData = MutableLiveData<DataState<BaseDomain>>()

    init {
        getPlaceList()
    }

    private fun getPlaceList() {
        viewModelScope.launch {
            getPlacesInteractor.call().onEach {
                placeListLiveData.value = it
            }.launchIn(viewModelScope)
        }

    }

}