package com.parsadehghan.divar.base.error_handler

import com.parsadehghan.divar.base.error_handler.ErrorHandler
import com.parsadehghan.divar.base.error_handler.IError
import com.parsadehghan.divar.base.error_handler.InternalError

class InternalErrorHandler(val errorCode:Int): ErrorHandler() {
    override fun createErrorStatus(): IError {
      return InternalError(errorCode)
    }
}