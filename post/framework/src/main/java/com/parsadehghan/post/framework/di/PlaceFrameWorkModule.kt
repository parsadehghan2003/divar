package com.parsadehghan.post.framework.di

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.parsadehghan.post.framework.PostLocalServiceImpl
import com.parsadehghan.post.framework.PlaceRemoteServiceImpl
import com.parsadehghan.divar.post.data_source.service.PostLocalService
import com.parsadehghan.divar.post.data_source.service.PostRemoteService
import com.parsadehghan.divar.database.data_source.service.PlaceDataStorage
import com.parsadehghan.divar.database.data_source.service.PostDataStorage
import com.parsadehghan.divar.gateway.DivarApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped


@Module
@InstallIn(ViewModelComponent::class)
object PlaceFrameWorkModule {
    const val PLACE_PREFERENCES = "place_preferences"

    @Provides
    @ViewModelScoped
    fun providePlaceLocalService(
        postDataStorage: PostDataStorage,
        dataStore: DataStore<Preferences>
    ): PostLocalService {
        return PostLocalServiceImpl(postDataStorage, dataStore)
    }

    @Provides
    @ViewModelScoped
    fun providePlaceRemoteService(divarApiService: DivarApiService): PostRemoteService {
        return PlaceRemoteServiceImpl(divarApiService)
    }

}