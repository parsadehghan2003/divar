package com.parsadehghan.divar.base.error_handler

enum class ErrorType {
    HTTP_ERROR,
    INTERNAL_ERROR,
    NOT_DEFINED
}