package com.parsadehghan.divar.post.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.parsadehghan.divar.base.models.PostObject
import com.parsadehghan.divar.base_android.TextCell
import kotlinx.android.synthetic.main.post_cell.view.districtTextView
import kotlinx.android.synthetic.main.post_cell.view.imageView
import kotlinx.android.synthetic.main.post_cell.view.priceTextView
import kotlinx.android.synthetic.main.post_cell.view.titleTextView
import net.iGap.base_android.util.adapter_utils.Change

class PostsAdapter(val context: Context) : ListAdapter<PostObject, PostsAdapter.ViewHolder>(
    AsyncDifferConfig.Builder(PlacesDiffUtil()).build()
), Filterable {

    var postFilterList = mutableListOf<PostObject>()

    private class PlacesDiffUtil : DiffUtil.ItemCallback<PostObject>() {
        override fun areItemsTheSame(oldItem: PostObject, newItem: PostObject): Boolean {
            return oldItem.postData?.token == newItem.postData?.token
        }

        override fun areContentsTheSame(oldItem: PostObject, newItem: PostObject): Boolean {
            return oldItem == newItem
        }

        override fun getChangePayload(oldItem: PostObject, newItem: PostObject): Any {
            return Change(
                oldItem, newItem
            )
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == 0)
            ViewHolder(LayoutInflater.from(context).inflate(R.layout.post_cell, parent, false))
        else ViewHolder(TextCell(context))


    }

    override fun getItemCount(): Int {
        return currentList.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (currentList[position].widgetType == "POST_ROW")
            0
        else
            1
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (holder.itemViewType == 0) {
            Glide.with(context).load(currentList[position].postData?.thumbnail).into(holder.imageView)
            holder.bindPostCell(currentList[position])
        } else {
            holder.bindAddCell(currentList[position])
        }
    }

    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val imageView: AppCompatImageView = view.imageView
        private val titleTextView = view.titleTextView
        private val priceTextView = view.priceTextView
        private val districtTextView = view.districtTextView
        fun bindPostCell(postObject: PostObject) {
            if (itemViewType == 0)
                titleTextView.text = postObject.postData?.title
            priceTextView.text = postObject.postData?.price
            districtTextView.text = postObject.postData?.district
        }

        fun bindAddCell(postObject: PostObject) {
            (view as TextCell).setText(postObject.postData?.text, true)
        }


    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    postFilterList = currentList
                } else {
                    val resultList = ArrayList<PostObject>()
                    for (row in currentList) {
                        if (row.postData?.title?.lowercase()
                                ?.contains(constraint.toString().lowercase()) == true
                        ) {
                            resultList.add(row)
                        }
                    }
                    postFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = postFilterList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                postFilterList = results?.values as MutableList<PostObject>
                submitList(postFilterList)
            }
        }
    }
}