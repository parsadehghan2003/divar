package com.parsadehghan.divar.base.models

import com.google.gson.annotations.SerializedName

data class PostsObject(
    @SerializedName(value="widget_list")
    val list: MutableList<PostObject> = mutableListOf()
)
