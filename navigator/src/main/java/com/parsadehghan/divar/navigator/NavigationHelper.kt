package com.parsadehghan.divar.navigator

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch

object NavigationHelper {

    // TODO: This key is initialized in the navigation helper class because both of the twoStepCheckPasswordFragment and LoginByEmailTokenFragment should access to this key
    const val KEY_IS_USER_LOGGED_IN = "net.iGap.navigator.isUserLoggedInKey"
    const val KEY_TWO_STEP_PASSWORD = "net.iGap.navigator.twoStepPasswordKey"
    const val KEY_EDITOR_OPTIONS = "net.iGap.navigator.editorOptionsKey"
    const val KEY_CHANGE_PHONE_NUMBER = "net.iGap.navigator.changePhoneNumber"

    val navigationFlow = MutableSharedFlow<NavigationModel>()
    val bottomNavigationHideStateFlow = MutableSharedFlow<Boolean>()
    fun navigateToDestination(
        destinationFragment: DestinationFragment,
        replace: Boolean,
        addToBackStack: Boolean,
        shouldBeVisible : Boolean = false,
        arg: Map<String, Any>? = null
    ) {
        CoroutineScope(Dispatchers.Main).launch {
            navigationFlow.emit(
                NavigationModel(
                    destinationFragment,
                    replace,
                    addToBackStack,
                    shouldBeVisible,
                    arg
                )
            )
        }
    }

    fun isNeedToNavigate(isNeedToNavigate:Boolean){
        CoroutineScope(Dispatchers.Main).launch {
            bottomNavigationHideStateFlow.emit(
                isNeedToNavigate
            )
        }
    }
}