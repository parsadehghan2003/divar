package com.parsadehghan.divar.city.ui.di

import com.parsadehghan.city.usecase.GetPlacesInteractor
import com.parsadehghan.divar.city.data_source.repository.PlaceRepository
import com.parsadehghan.divar.city.data_source.repository.PlaceRepositoryImpl
import com.parsadehghan.divar.city.data_source.service.PlaceLocalService
import com.parsadehghan.divar.city.data_source.service.PlaceRemoteService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(ViewModelComponent::class)
object PlaceModule {
    @Provides
    @ViewModelScoped
    fun providePlaceRepository(
        placeLocalService: PlaceLocalService,
        placeRemoteService: PlaceRemoteService
    ): PlaceRepository {
        return PlaceRepositoryImpl(placeLocalService, placeRemoteService)
    }
    @Provides
    @ViewModelScoped
    fun provideGetPlacesInteractor(
        placeRepository: PlaceRepository
    ): GetPlacesInteractor {
        return GetPlacesInteractor(placeRepository)
    }
}