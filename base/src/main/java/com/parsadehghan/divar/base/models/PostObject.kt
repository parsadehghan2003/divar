package com.parsadehghan.divar.base.models

import com.google.gson.annotations.SerializedName

data class PostObject(
    @SerializedName(value = "widget_type")
    var widgetType: String = "",

    @SerializedName(value = "data")
    var postData: PostData? = null,


)