package com.parsadehghan.divar

import android.app.Application
import android.content.SharedPreferences
import dagger.hilt.android.HiltAndroidApp
import java.io.File


@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()
    }
}