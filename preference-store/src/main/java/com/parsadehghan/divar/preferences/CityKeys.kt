package com.parsadehghan.divar.preferences

import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey

object CityKeys {

    val currentCityId = intPreferencesKey("currentCityId")
    val currentCityName = stringPreferencesKey("currentCityName")
}