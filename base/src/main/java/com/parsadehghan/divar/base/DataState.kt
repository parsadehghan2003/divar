package com.parsadehghan.divar.base

import com.parsadehghan.divar.base.error_handler.ErrorModel

sealed class DataState<T> {

    data class Error<T>(
        val errorObject: ErrorModel,
        var data: T? = null
    ) : DataState<T>()


    data class Data<T>(
        val data: T? = null
    ) : DataState<T>()

}
