package com.parsadehghan.divar.city.ui

import android.content.Context
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.parsadehghan.divar.city.TextCell
import com.parsadehghan.divar.base.models.PlaceObject
import com.parsadehghan.divar.navigator.DestinationFragment
import com.parsadehghan.divar.navigator.NavigationHelper
import net.iGap.base_android.util.adapter_utils.Change

class PlacesAdapter(context: Context) : ListAdapter<PlaceObject, PlacesAdapter.ViewHolder>(
    AsyncDifferConfig.Builder(PlacesDiffUtil()).build()
) , Filterable {

    var placeFilterList = mutableListOf<PlaceObject>()

    private class PlacesDiffUtil : DiffUtil.ItemCallback<PlaceObject>() {
        override fun areItemsTheSame(oldItem: PlaceObject, newItem: PlaceObject): Boolean {
            return oldItem.id.hashCode() == newItem.id.hashCode()
        }

        override fun areContentsTheSame(oldItem: PlaceObject, newItem: PlaceObject): Boolean {
            return oldItem == newItem
        }

        override fun getChangePayload(oldItem: PlaceObject, newItem: PlaceObject): Any {
            return Change(
                oldItem, newItem
            )
        }

    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(TextCell(parent.context))
    }

    override fun getItemCount(): Int {
        return currentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    class ViewHolder(private val view: TextCell) : RecyclerView.ViewHolder(view) {
        fun bind(placeObject: PlaceObject) {
            view.setText(placeObject.name,true)
            view.setOnClickListener {
                val args = mutableMapOf<String,Any>()
                args["CITY_OBJECT_KEY"] = placeObject.id
                NavigationHelper.navigateToDestination(
                    DestinationFragment.POSTS_FRAGMENT,
                    replace = true,
                    addToBackStack = true,
                    arg = args
                )
            }
        }
    }
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    placeFilterList = currentList
                } else {
                    val resultList = ArrayList<PlaceObject>()
                    for (row in currentList) {
                        if (row.name.lowercase().contains(constraint.toString().lowercase())) {
                            resultList.add(row)
                        }
                    }
                    placeFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = placeFilterList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                placeFilterList = results?.values as MutableList<PlaceObject>
                submitList(placeFilterList)
            }
        }
    }
}