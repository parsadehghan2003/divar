package com.parsadehghan.post.framework

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState
import com.parsadehghan.divar.base.error_handler.dataStateInternalErrorHandler
import com.parsadehghan.divar.base.models.City
import com.parsadehghan.divar.post.data_source.service.PostLocalService
import com.parsadehghan.divar.post.domain.CRUDCityObject
import com.parsadehghan.divar.post.domain.GetPostsObject
import com.parsadehghan.divar.database.data_source.service.PostDataStorage
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import com.parsadehghan.divar.preferences.cityId
import com.parsadehghan.divar.preferences.currentCityName
import javax.inject.Inject

class PostLocalServiceImpl @Inject constructor(
    private val postDataStorage: PostDataStorage,
    private val dataStore: DataStore<Preferences>
) : PostLocalService {
    override suspend fun readPosts(): DataState<BaseDomain> {
        val realmList = postDataStorage.readPosts()
        return if (realmList != null)
            DataState.Data(
                GetPostsObject.GetPostsObjectResponse(
                    createPostsObjectWithPostRealmList(realmList)
                )
            )
        else dataStateInternalErrorHandler(1)
    }

    override suspend fun insertPosts(baseDomain: BaseDomain): DataState<BaseDomain> {
        val realmList = postDataStorage.insertOrUpdatePosts(
            createPostRealmListWithPostsObject(
                (baseDomain as GetPostsObject.GetPostsObjectResponse).posts
            )
        )
        return if (realmList != null)
            DataState.Data(
                GetPostsObject.GetPostsObjectResponse(
                    createPostsObjectWithPostRealmList(realmList)
                )
            )
        else dataStateInternalErrorHandler(1)
    }

    override suspend fun readCurrentPlace(): DataState<BaseDomain> {
        return dataStore.data.catch {
            dataStateInternalErrorHandler(5)
        }.map {
            DataState.Data(
                CRUDCityObject.GetCityObjectResponse(
                    City(it.cityId!!, it.currentCityName!!)
                ) as BaseDomain
            )
        }
            .first()
    }
}