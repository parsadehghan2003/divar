package com.parsadehghan.city.framework

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState
import com.parsadehghan.divar.base.error_handler.dataStateInternalErrorHandler
import com.parsadehghan.divar.base.models.City
import com.parsadehghan.divar.city.data_source.service.PlaceLocalService
import com.parsadehghan.divar.city.domain.CRUDCityObject
import com.parsadehghan.divar.city.domain.GetPlacesObject
import com.parsadehghan.divar.database.data_source.service.PlaceDataStorage
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import com.parsadehghan.divar.preferences.cityId
import com.parsadehghan.divar.preferences.currentCityName
import javax.inject.Inject

class PlaceLocalServiceImpl @Inject constructor(
    val placeDataStorage: PlaceDataStorage,
    val dataStore: DataStore<Preferences>
) : PlaceLocalService {
    override suspend fun readPlaces(): DataState<BaseDomain> {
        val realmList = placeDataStorage.readPlaces()
        return if (realmList != null)
            DataState.Data(
                GetPlacesObject.GetPlacesObjectResponse(
                    createPlaceListListWithPlaceRealmList(realmList)
                )
            )
        else dataStateInternalErrorHandler(1)
    }

    override suspend fun insertPlaces(baseDomain: BaseDomain): DataState<BaseDomain> {
        val realmList = placeDataStorage.insertOrUpdatePlaces(
            createPlaceRealmListWithPlaceObjectList(
                (baseDomain as GetPlacesObject.GetPlacesObjectResponse).placeObjects
            )
        )
        return if (realmList != null)
            DataState.Data(
                GetPlacesObject.GetPlacesObjectResponse(
                    createPlaceListListWithPlaceRealmList(realmList)
                )
            )
        else dataStateInternalErrorHandler(1)
    }

    override suspend fun readCurrentPlace(): DataState<BaseDomain> {
        return dataStore.data.catch {
            dataStateInternalErrorHandler(5)
        }.map {
            DataState.Data(
                CRUDCityObject.GetCityObjectResponse(
                    City(it.cityId!!, it.currentCityName!!)
                ) as BaseDomain
            )
        }
            .first()
    }
}