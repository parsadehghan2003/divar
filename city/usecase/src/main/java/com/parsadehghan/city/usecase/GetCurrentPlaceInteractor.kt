package com.parsadehghan.city.usecase

import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState
import com.parsadehghan.divar.base.interactor.UseCase
import com.parsadehghan.divar.city.data_source.repository.PlaceRepository

class GetCurrentPlaceInteractor(val placeRepository: PlaceRepository) : UseCase<Void>() {
    override suspend fun run(params: Void?): DataState<BaseDomain> {
        return placeRepository.getCurrentPlace()
    }


}