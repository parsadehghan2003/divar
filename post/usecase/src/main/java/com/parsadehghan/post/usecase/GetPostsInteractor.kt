package com.parsadehghan.post.usecase

import com.parsadehghan.divar.base.BaseDomain
import com.parsadehghan.divar.base.DataState
import com.parsadehghan.divar.base.interactor.UseCase
import com.parsadehghan.divar.post.data_source.repository.PostRepository

class GetPostsInteractor(private val postRepository: PostRepository) : UseCase<BaseDomain>() {
    override suspend fun run(params: BaseDomain?): DataState<BaseDomain> {
        return postRepository.getPosts(params!!)
    }

}