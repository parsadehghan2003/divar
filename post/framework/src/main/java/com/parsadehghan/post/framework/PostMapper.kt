package com.parsadehghan.post.framework

import com.parsadehghan.divar.base.models.PostData
import com.parsadehghan.divar.base.models.PostObject
import com.parsadehghan.divar.base.models.PostsObject
import com.parsadehghan.divar.database.domain.RealmPost
import io.realm.kotlin.ext.realmListOf
import io.realm.kotlin.types.RealmList


fun createPostObjectWithRealmPost(realmObject: RealmPost): PostObject {
    return PostObject().apply {
        postData = PostData().apply {
            title = realmObject.title
            token = realmObject.token
            price = realmObject.price
            thumbnail = realmObject.thumbnail
            city = realmObject.city
            district = realmObject.district
            text = realmObject.text
        }
        widgetType = realmObject.widgetType
    }
}

fun createRealmPostWithPostObject(placeObject: PostObject): RealmPost {
    return RealmPost().apply {
        placeObject.postData?.let {
            title = it.title
            token = it.token
            price = it.price
            thumbnail = it.thumbnail
            city = it.city
            district = it.district
            text = it.text
        }
        widgetType = placeObject.widgetType
    }
}

fun createPostRealmListWithPostsObject(list: PostsObject): RealmList<RealmPost> {
    val realmPlaces = realmListOf<RealmPost>()
    list.list.map {
        realmPlaces.add(
            createRealmPostWithPostObject(it)
        )
    }
    return realmPlaces
}


fun createPostsObjectWithPostRealmList(list: RealmList<RealmPost>): PostsObject {
    val placeObjects = PostsObject()
    list.map {
        placeObjects.list.add(
            createPostObjectWithRealmPost(it)
        )
    }
    return placeObjects
}
